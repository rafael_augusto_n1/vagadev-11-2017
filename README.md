# Avaliação Front-End N1 #

Nossa avaliação é bem simples, pois o intuito é analisar como o candidato desenvolve os itens solitados visando qualidade de código, automatizadores, preprocessadores e etc.

### Requisitos para essa avalição ###

* O projeto deve ser responsivo. Lembrando que no PSD só existe o layout na versão desktop;
* A prateleira de novidades deve ser feita usando dados do arquivo ".json", que também esta no repositório;
* Crie uma branch com seu nome e ao fim do desenvolvimento crie um pull request;
* Interações e funcionalidades não sugeridas no layout serão levadas em consideração;
* Prazo para entrega da avaliação: 07/11/2017.

OBS.: O candidato esta livre para trabalhar com a estrutura e tecnologia que preferir.